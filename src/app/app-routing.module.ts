import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DatenschutzerklaerungComponent } from "./components/datenschutzerklaerung/datenschutzerklaerung.component";
import { ImpressumComponent } from "./components/impressum/impressum.component";
import { StartseiteComponent } from "./components/startseite/startseite.component";
import { Termine2016Component } from "./components/termine/termine2016/termine2016.component";
import { Termine2017Component } from "./components/termine/termine2017/termine2017.component";
import { Termine2018Component } from "./components/termine/termine2018/termine2018.component";
import { Termine2020Component } from "./components/termine/termine2020/termine2020.component";
import { Termine2021Component } from "./components/termine/termine2021/termine2021.component";
import { TreffenBacksbergComponent } from "./components/treffen-backsberg/treffen-backsberg.component";
import { Backsberg2004Component } from "./components/treffen/backsberg2004/backsberg2004.component";
import { Backsberg2005Component } from "./components/treffen/backsberg2005/backsberg2005.component";
import { Backsberg2006Component } from "./components/treffen/backsberg2006/backsberg2006.component";
import { Backsberg2007Component } from "./components/treffen/backsberg2007/backsberg2007.component";
import { Backsberg2008Component } from "./components/treffen/backsberg2008/backsberg2008.component";
import { Backsberg2009Component } from "./components/treffen/backsberg2009/backsberg2009.component";
import { Backsberg2010Component } from "./components/treffen/backsberg2010/backsberg2010.component";
import { Backsberg2011Component } from "./components/treffen/backsberg2011/backsberg2011.component";
import { Backsberg2012Component } from "./components/treffen/backsberg2012/backsberg2012.component";
import { Backsberg2013Component } from "./components/treffen/backsberg2013/backsberg2013.component";
import { Backsberg2014Component } from "./components/treffen/backsberg2014/backsberg2014.component";
import { Backsberg2016Component } from "./components/treffen/backsberg2016/backsberg2016.component";
import { Backsberg2018Component } from "./components/treffen/backsberg2018/backsberg2018.component";
import { AnfahrtComponent } from "./components/anfahrt/anfahrt.component";
import { GaestebuchComponent } from "./components/gaestebuch/gaestebuch.component";
import { KontaktComponent } from "./components/kontakt/kontakt.component";
import { NichtGefundenComponent } from "./components/nicht-gefunden/nicht-gefunden.component";
import { MopedsComponent } from "./components/mopeds/mopeds.component";
import { Pfingsten2018Component } from "./components/ausfahrten/pfingsten2018/pfingsten2018.component";
import { LinksComponent } from "./components/links/links.component";
import { Termine2022Component } from "./components/termine/termine2022/termine2022.component";

const routes: Routes = [
  {
    path: "start",
    component: StartseiteComponent,
    data: { title: "Bremer-Italo-Club" },
  },
  {
    path: "termine",
    redirectTo: "/termine/2020",
    pathMatch: "full",
  },
  {
    path: "termine/2016",
    component: Termine2016Component,
    data: { title: "Termine 2016 - Bremer-Italo-Club" },
  },
  {
    path: "termine/2017",
    component: Termine2017Component,
    data: { title: "Termine 2017 - Bremer-Italo-Club" },
  },
  {
    path: "termine/2018",
    component: Termine2018Component,
    data: { title: "Termine 2018 - Bremer-Italo-Club" },
  },
  {
    path: "termine/2020",
    component: Termine2020Component,
    data: { title: "Termine 2020 - Bremer-Italo-Club" },
  },
  {
    path: "termine/2021",
    component: Termine2021Component,
    data: { title: "Termine 2022 - Bremer-Italo-Club" },
  },
  {
    path: "termine/2022",
    component: Termine2022Component,
    data: { title: "Termine 2022 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/einladung",
    component: TreffenBacksbergComponent,
    data: { title: "Treffen Einladung - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2004",
    component: Backsberg2004Component,
    data: { title: "Treffen 2004 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2005",
    component: Backsberg2005Component,
    data: { title: "Treffen 2005 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2006",
    component: Backsberg2006Component,
    data: { title: "Treffen 2006 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2007",
    component: Backsberg2007Component,
    data: { title: "Treffen 2007 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2008",
    component: Backsberg2008Component,
    data: { title: "Treffen 2008 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2009",
    component: Backsberg2009Component,
    data: { title: "Treffen 2009 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2010",
    component: Backsberg2010Component,
    data: { title: "Treffen 2010 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2011",
    component: Backsberg2011Component,
    data: { title: "Treffen 2011 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2012",
    component: Backsberg2012Component,
    data: { title: "Treffen 2012 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2013",
    component: Backsberg2013Component,
    data: { title: "Treffen 2013 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2014",
    component: Backsberg2014Component,
    data: { title: "Treffen 2014 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2016",
    component: Backsberg2016Component,
    data: { title: "Treffen 2016 - Bremer-Italo-Club" },
  },
  {
    path: "treffen/2018",
    component: Backsberg2018Component,
    data: { title: "Treffen 2018 - Bremer-Italo-Club" },
  },
  {
    path: "ausfahrten/2018/pfingsten",
    component: Pfingsten2018Component,
    data: { title: "Pfingstausfahrt 2018 - Bremer-Italo-Club" },
  },
  {
    path: "mopeds",
    component: MopedsComponent,
    data: { title: "Unsere Mopeds - Bremer-Italo-Club" },
  },
  {
    path: "anfahrt",
    component: AnfahrtComponent,
    data: { title: "Anfahrt - Bremer-Italo-Club" },
  },
  {
    path: "links",
    component: LinksComponent,
    data: { title: "Links - Bremer-Italo-Club" },
  },
  {
    path: "gaestebuch",
    component: GaestebuchComponent,
    data: { title: "Gästebuch - Bremer-Italo-Club" },
  },
  {
    path: "kontakt",
    component: KontaktComponent,
    data: { title: "Kontakt - Bremer-Italo-Club" },
  },
  {
    path: "impressum",
    component: ImpressumComponent,
    data: { title: "Impressum - Bremer-Italo-Club" },
  },
  {
    path: "datenschutzerklaerung",
    component: DatenschutzerklaerungComponent,
    data: { title: "Datenschutzerklärung - Bremer-Italo-Club" },
  },
  {
    path: "",
    redirectTo: "start",
    pathMatch: "full",
  },
  {
    path: "**",
    component: NichtGefundenComponent,
    data: { title: "404 - Bremer-Italo-Club" },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
