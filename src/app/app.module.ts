import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { Angular2ImageGalleryModule } from 'angular2-image-gallery';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2Piwik } from 'angulartics2/piwik';
import { DeviceDetectorModule } from 'ngx-device-detector';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartseiteComponent } from './components/startseite/startseite.component';
import { Termine2016Component } from './components/termine/termine2016/termine2016.component';
import { Termine2017Component } from './components/termine/termine2017/termine2017.component';
import { Termine2018Component } from './components/termine/termine2018/termine2018.component';
import { Termine2020Component } from './components/termine/termine2020/termine2020.component';
import { Termine2021Component } from './components/termine/termine2021/termine2021.component';
import { TreffenBacksbergComponent } from './components/treffen-backsberg/treffen-backsberg.component';
import { Backsberg2004Component } from './components/treffen/backsberg2004/backsberg2004.component';
import { Backsberg2005Component } from './components/treffen/backsberg2005/backsberg2005.component';
import { Backsberg2006Component } from './components/treffen/backsberg2006/backsberg2006.component';
import { Backsberg2007Component } from './components/treffen/backsberg2007/backsberg2007.component';
import { Backsberg2008Component } from './components/treffen/backsberg2008/backsberg2008.component';
import { Backsberg2009Component } from './components/treffen/backsberg2009/backsberg2009.component';
import { Backsberg2010Component } from './components/treffen/backsberg2010/backsberg2010.component';
import { Backsberg2011Component } from './components/treffen/backsberg2011/backsberg2011.component';
import { Backsberg2012Component } from './components/treffen/backsberg2012/backsberg2012.component';
import { Backsberg2013Component } from './components/treffen/backsberg2013/backsberg2013.component';
import { Backsberg2014Component } from './components/treffen/backsberg2014/backsberg2014.component';
import { Backsberg2016Component } from './components/treffen/backsberg2016/backsberg2016.component';
import { Backsberg2018Component } from './components/treffen/backsberg2018/backsberg2018.component';
import { AnfahrtComponent } from './components/anfahrt/anfahrt.component';
import { LinksComponent } from './components/links/links.component';
import { GaestebuchComponent } from './components/gaestebuch/gaestebuch.component';
import { KontaktComponent } from './components/kontakt/kontakt.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { DatenschutzerklaerungComponent } from './components/datenschutzerklaerung/datenschutzerklaerung.component';
import { NichtGefundenComponent } from './components/nicht-gefunden/nicht-gefunden.component';
import { MopedsComponent } from './components/mopeds/mopeds.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { Pfingsten2018Component } from './components/ausfahrten/pfingsten2018/pfingsten2018.component';
import { Termine2022Component } from './components/termine/termine2022/termine2022.component';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'www.bremer-italo-club.de'
  },
  palette: {
    popup: {
      background: '#eeeeee',
      text: '#000000'
    },
    button: {
      background: '#007e33',
      text: '#ffffff'
    }
  },
  theme: 'edgeless',
  position: 'bottom-right',
  content: {
    message: 'Die Webseite vom B.I.C benutzt Cookies, welche zur Verbesserung der Seite dienen. Wenn du unsere Seite benutzt stimmst du dieser Verwendung zu.',
    dismiss: 'Ok!',
    link: 'Zur Datenschutzerklärung',
    href: 'datenschutzerklaerung'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    StartseiteComponent,
    Termine2016Component,
    Termine2017Component,
    Termine2018Component,
    Termine2020Component,
    Termine2021Component,
    TreffenBacksbergComponent,
    Backsberg2004Component,
    Backsberg2005Component,
    Backsberg2006Component,
    Backsberg2007Component,
    Backsberg2008Component,
    Backsberg2009Component,
    Backsberg2010Component,
    Backsberg2011Component,
    Backsberg2012Component,
    Backsberg2013Component,
    Backsberg2014Component,
    Backsberg2016Component,
    Backsberg2018Component,
    AnfahrtComponent,
    LinksComponent,
    GaestebuchComponent,
    KontaktComponent,
    ImpressumComponent,
    DatenschutzerklaerungComponent,
    NichtGefundenComponent,
    MopedsComponent,
    FooterComponent,
    NavigationComponent,
    Pfingsten2018Component,
    Termine2022Component
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    Angular2ImageGalleryModule,
    NgcCookieConsentModule.forRoot(cookieConfig),
    Angulartics2Module.forRoot(),
    DeviceDetectorModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
