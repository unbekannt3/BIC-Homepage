import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-gaestebuch',
  templateUrl: './gaestebuch.component.html',
  styleUrls: ['./gaestebuch.component.scss']
})
export class GaestebuchComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Gästebuch - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Gästebuch' },
      { name: 'description', content: 'Gästebuch vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
