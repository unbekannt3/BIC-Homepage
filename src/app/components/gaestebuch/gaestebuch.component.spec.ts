import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaestebuchComponent } from './gaestebuch.component';

describe('GaestebuchComponent', () => {
  let component: GaestebuchComponent;
  let fixture: ComponentFixture<GaestebuchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaestebuchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaestebuchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
