import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2012',
  templateUrl: './backsberg2012.component.html',
  styleUrls: ['./backsberg2012.component.scss']
})
export class Backsberg2012Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2012 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2012' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2012' }
    ]);

  }

  ngOnInit() {
  }

}
