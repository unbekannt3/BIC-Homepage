import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2012Component } from './backsberg2012.component';

describe('Backsberg2012Component', () => {
  let component: Backsberg2012Component;
  let fixture: ComponentFixture<Backsberg2012Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2012Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2012Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
