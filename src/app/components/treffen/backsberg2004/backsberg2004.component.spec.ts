import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2004Component } from './backsberg2004.component';

describe('Backsberg2004Component', () => {
  let component: Backsberg2004Component;
  let fixture: ComponentFixture<Backsberg2004Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2004Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
