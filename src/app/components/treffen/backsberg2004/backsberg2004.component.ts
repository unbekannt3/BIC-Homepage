import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2004',
  templateUrl: './backsberg2004.component.html',
  styleUrls: ['./backsberg2004.component.scss']
})
export class Backsberg2004Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2004 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2004' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2004' }
    ]);

  }

  ngOnInit() {
  }

}
