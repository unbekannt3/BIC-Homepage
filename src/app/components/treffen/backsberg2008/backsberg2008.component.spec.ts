import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2008Component } from './backsberg2008.component';

describe('Backsberg2008Component', () => {
  let component: Backsberg2008Component;
  let fixture: ComponentFixture<Backsberg2008Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2008Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2008Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
