import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2008',
  templateUrl: './backsberg2008.component.html',
  styleUrls: ['./backsberg2008.component.scss']
})
export class Backsberg2008Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2008 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2008' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2008' }
    ]);

  }

  ngOnInit() {
  }

}
