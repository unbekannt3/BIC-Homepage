import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2010',
  templateUrl: './backsberg2010.component.html',
  styleUrls: ['./backsberg2010.component.scss']
})
export class Backsberg2010Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2010 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2010' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2010' }
    ]);

  }

  ngOnInit() {
  }

}
