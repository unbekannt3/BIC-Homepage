import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2010Component } from './backsberg2010.component';

describe('Backsberg2010Component', () => {
  let component: Backsberg2010Component;
  let fixture: ComponentFixture<Backsberg2010Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2010Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2010Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
