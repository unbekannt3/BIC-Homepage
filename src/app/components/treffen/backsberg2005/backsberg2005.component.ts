import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2005',
  templateUrl: './backsberg2005.component.html',
  styleUrls: ['./backsberg2005.component.scss']
})
export class Backsberg2005Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2005 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2005' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2005' }
    ]);

  }

  ngOnInit() {
  }

}
