import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2005Component } from './backsberg2005.component';

describe('Backsberg2005Component', () => {
  let component: Backsberg2005Component;
  let fixture: ComponentFixture<Backsberg2005Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2005Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2005Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
