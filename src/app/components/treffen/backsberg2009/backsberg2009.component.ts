import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2009',
  templateUrl: './backsberg2009.component.html',
  styleUrls: ['./backsberg2009.component.scss']
})
export class Backsberg2009Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2009 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2009' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2009' }
    ]);

  }

  ngOnInit() {
  }

}
