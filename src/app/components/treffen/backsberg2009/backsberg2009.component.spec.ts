import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2009Component } from './backsberg2009.component';

describe('Backsberg2009Component', () => {
  let component: Backsberg2009Component;
  let fixture: ComponentFixture<Backsberg2009Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2009Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2009Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
