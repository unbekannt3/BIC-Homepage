import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2018',
  templateUrl: './backsberg2018.component.html',
  styleUrls: ['./backsberg2018.component.scss']
})
export class Backsberg2018Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2018 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2018, 26. Treffen, Jubiläum' },
      { name: 'description', content: 'Bilder vom 26. Treffen am Backsberg 2018' }
    ]);

  }

  ngOnInit() {
  }

}
