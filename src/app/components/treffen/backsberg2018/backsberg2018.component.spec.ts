import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2018Component } from './backsberg2018.component';

describe('Backsberg2018Component', () => {
  let component: Backsberg2018Component;
  let fixture: ComponentFixture<Backsberg2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
