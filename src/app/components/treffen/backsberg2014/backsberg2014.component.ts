import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2014',
  templateUrl: './backsberg2014.component.html',
  styleUrls: ['./backsberg2014.component.scss']
})
export class Backsberg2014Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2014 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2014' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2014' }
    ]);

  }

  ngOnInit() {
  }

}
