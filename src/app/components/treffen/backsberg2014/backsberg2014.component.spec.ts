import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2014Component } from './backsberg2014.component';

describe('Backsberg2014Component', () => {
  let component: Backsberg2014Component;
  let fixture: ComponentFixture<Backsberg2014Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2014Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2014Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
