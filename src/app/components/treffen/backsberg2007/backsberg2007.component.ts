import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2007',
  templateUrl: './backsberg2007.component.html',
  styleUrls: ['./backsberg2007.component.scss']
})
export class Backsberg2007Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2007 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2007' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2007' }
    ]);

  }

  ngOnInit() {
  }

}
