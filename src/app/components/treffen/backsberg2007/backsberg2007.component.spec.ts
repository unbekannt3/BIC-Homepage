import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2007Component } from './backsberg2007.component';

describe('Backsberg2007Component', () => {
  let component: Backsberg2007Component;
  let fixture: ComponentFixture<Backsberg2007Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2007Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2007Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
