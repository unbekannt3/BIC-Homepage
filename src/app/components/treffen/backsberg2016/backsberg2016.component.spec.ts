import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2016Component } from './backsberg2016.component';

describe('Backsberg2016Component', () => {
  let component: Backsberg2016Component;
  let fixture: ComponentFixture<Backsberg2016Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2016Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2016Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
