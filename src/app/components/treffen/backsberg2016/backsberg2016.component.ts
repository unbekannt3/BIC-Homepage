import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2016',
  templateUrl: './backsberg2016.component.html',
  styleUrls: ['./backsberg2016.component.scss']
})
export class Backsberg2016Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2016 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2016, 25. Treffen, Jubiläum' },
      { name: 'description', content: 'Bilder vom 25. Treffen am Backsberg 2016' }
    ]);

  }

  ngOnInit() {
  }

}
