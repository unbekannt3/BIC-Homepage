import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2006Component } from './backsberg2006.component';

describe('Backsberg2006Component', () => {
  let component: Backsberg2006Component;
  let fixture: ComponentFixture<Backsberg2006Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2006Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2006Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
