import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2006',
  templateUrl: './backsberg2006.component.html',
  styleUrls: ['./backsberg2006.component.scss']
})
export class Backsberg2006Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2006 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2006' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2006' }
    ]);

  }

  ngOnInit() {
  }

}
