import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2013Component } from './backsberg2013.component';

describe('Backsberg2013Component', () => {
  let component: Backsberg2013Component;
  let fixture: ComponentFixture<Backsberg2013Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2013Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2013Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
