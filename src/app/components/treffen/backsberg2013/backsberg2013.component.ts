import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2013',
  templateUrl: './backsberg2013.component.html',
  styleUrls: ['./backsberg2013.component.scss']
})
export class Backsberg2013Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2013 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2013' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2013' }
    ]);

  }

  ngOnInit() {
  }

}
