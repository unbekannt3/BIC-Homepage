import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-backsberg2011',
  templateUrl: './backsberg2011.component.html',
  styleUrls: ['./backsberg2011.component.scss']
})
export class Backsberg2011Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bilder 2011 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Backsberg 2011' },
      { name: 'description', content: 'Bilder vom Treffen am Backsberg 2011' }
    ]);

  }

  ngOnInit() {
  }

}
