import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Backsberg2011Component } from './backsberg2011.component';

describe('Backsberg2011Component', () => {
  let component: Backsberg2011Component;
  let fixture: ComponentFixture<Backsberg2011Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Backsberg2011Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Backsberg2011Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
