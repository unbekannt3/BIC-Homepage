import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-mopeds',
  templateUrl: './mopeds.component.html',
  styleUrls: ['./mopeds.component.scss']
})
export class MopedsComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Mopeds - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Unsere Motorräder' },
      { name: 'description', content: 'Motorräder der BIC Mitglieder' }
    ]);

  }

  ngOnInit() {
  }

}
