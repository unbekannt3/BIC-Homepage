import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-pfingsten2018',
  templateUrl: './pfingsten2018.component.html',
  styleUrls: ['./pfingsten2018.component.scss']
})
export class Pfingsten2018Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Pfingstfahrt 2018 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Villa Löwenherz, Pfingstausfahrt, Pfingsten, 2018' },
      { name: 'description', content: 'Bilder von der Pfingstausfahrt zur Villa Löwenherz 2018' }
    ]);

  }

  ngOnInit() {
  }

}
