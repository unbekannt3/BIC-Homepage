import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pfingsten2018Component } from './pfingsten2018.component';

describe('Pfingsten2018Component', () => {
  let component: Pfingsten2018Component;
  let fixture: ComponentFixture<Pfingsten2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pfingsten2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pfingsten2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
