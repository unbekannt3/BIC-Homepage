import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-startseite',
  templateUrl: './startseite.component.html',
  styleUrls: ['./startseite.component.scss']
})
export class StartseiteComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Italiener Oyten, Moto Guzzi, BIC, Paulaner Wehrschloss, Italo, Italo Treffen, Startseite' },
      { name: 'description', content: 'Startseite von der BIC Homepage' }
    ]);

  }

  ngOnInit() {
  }

}
