import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-datenschutzerklaerung',
  templateUrl: './datenschutzerklaerung.component.html',
  styleUrls: ['./datenschutzerklaerung.component.scss']
})
export class DatenschutzerklaerungComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Datenschutzerklärung - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, DSGVO, GDPR, Datenschutzerklärung' },
      { name: 'description', content: 'Datenschutzerklärung vom Bremer-Italo-Club' }
    ]);

  }

  ngOnInit() {
  }

}
