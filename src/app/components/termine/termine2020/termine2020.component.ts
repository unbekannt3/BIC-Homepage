import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-termine2020',
  templateUrl: './termine2020.component.html',
  styleUrls: ['./termine2020.component.scss']
})
export class Termine2020Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Termine 2020 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termine 2020' },
      { name: 'description', content: 'Termine aus 2020 vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
