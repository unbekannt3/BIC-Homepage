import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2020Component } from './termine2020.component';

describe('Termine2020Component', () => {
  let component: Termine2020Component;
  let fixture: ComponentFixture<Termine2020Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2020Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2020Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
