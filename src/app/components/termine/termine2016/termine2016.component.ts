import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-termine2016',
  templateUrl: './termine2016.component.html',
  styleUrls: ['./termine2016.component.scss']
})
export class Termine2016Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Termine 2016 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termine 2016' },
      { name: 'description', content: 'Termine aus 2016 vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
