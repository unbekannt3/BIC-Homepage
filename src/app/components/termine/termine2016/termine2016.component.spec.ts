import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2016Component } from './termine2016.component';

describe('Termine2016Component', () => {
  let component: Termine2016Component;
  let fixture: ComponentFixture<Termine2016Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2016Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2016Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
