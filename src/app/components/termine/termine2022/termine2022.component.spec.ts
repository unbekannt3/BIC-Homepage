import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2022Component } from './termine2022.component';

describe('Termine2022Component', () => {
  let component: Termine2022Component;
  let fixture: ComponentFixture<Termine2022Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2022Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2022Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
