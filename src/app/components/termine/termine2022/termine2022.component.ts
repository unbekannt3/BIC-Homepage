import { Component, OnInit } from "@angular/core";
import { Meta, Title } from "@angular/platform-browser";

@Component({
  selector: "app-termine2022",
  templateUrl: "./termine2022.component.html",
  styleUrls: ["./termine2022.component.scss"],
})
export class Termine2022Component implements OnInit {
  constructor(meta: Meta, title: Title) {
    title.setTitle("Termine 2022 - Bremer-Italo-Club");

    meta.addTags([
      { name: "author", content: "unbekannt3" },
      {
        name: "keywords",
        content:
          "BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termine 2021",
      },
      { name: "description", content: "Termine aus 2022 vom BIC" },
    ]);
  }

  ngOnInit(): void {}
}
