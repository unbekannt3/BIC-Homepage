import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-termine2017',
  templateUrl: './termine2017.component.html',
  styleUrls: ['./termine2017.component.scss']
})
export class Termine2017Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Termine 2017 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termine 2017' },
      { name: 'description', content: 'Termine aus 2017 vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
