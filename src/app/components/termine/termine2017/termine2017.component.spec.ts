import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2017Component } from './termine2017.component';

describe('Termine2017Component', () => {
  let component: Termine2017Component;
  let fixture: ComponentFixture<Termine2017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
