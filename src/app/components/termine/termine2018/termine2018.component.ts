import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-termine2018',
  templateUrl: './termine2018.component.html',
  styleUrls: ['./termine2018.component.scss']
})
export class Termine2018Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Termine 2018 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termie 2018' },
      { name: 'description', content: 'Termine aus 2018 vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
