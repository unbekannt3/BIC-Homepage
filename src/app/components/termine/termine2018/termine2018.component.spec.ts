import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2018Component } from './termine2018.component';

describe('Termine2018Component', () => {
  let component: Termine2018Component;
  let fixture: ComponentFixture<Termine2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
