import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Termine2021Component } from './termine2021.component';

describe('Termine2021Component', () => {
  let component: Termine2021Component;
  let fixture: ComponentFixture<Termine2021Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Termine2021Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Termine2021Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
