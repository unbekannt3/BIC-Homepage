import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-termine2021',
  templateUrl: './termine2021.component.html',
  styleUrls: ['./termine2021.component.scss']
})
export class Termine2021Component implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Termine 2021 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Termine 2021' },
      { name: 'description', content: 'Termine aus 2021 vom BIC' }
    ]);

  }

  ngOnInit() {
  }

}
