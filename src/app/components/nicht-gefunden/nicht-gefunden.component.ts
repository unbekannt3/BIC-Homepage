import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-nicht-gefunden',
  templateUrl: './nicht-gefunden.component.html',
  styleUrls: ['./nicht-gefunden.component.scss']
})
export class NichtGefundenComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('404 - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: '404' },
      { name: 'keywords', content: 'Error, 404, Not Found, Not, Found' },
      { name: 'description', content: 'Nichts Gefunden - 404 Not Found' }
    ]);

  }

  ngOnInit() {
  }

}
