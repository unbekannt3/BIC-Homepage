import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreffenBacksbergComponent } from './treffen-backsberg.component';

describe('TreffenBacksbergComponent', () => {
  let component: TreffenBacksbergComponent;
  let fixture: ComponentFixture<TreffenBacksbergComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreffenBacksbergComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreffenBacksbergComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
