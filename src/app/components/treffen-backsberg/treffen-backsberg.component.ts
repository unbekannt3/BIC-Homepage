import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-treffen-backsberg',
  templateUrl: './treffen-backsberg.component.html',
  styleUrls: ['./treffen-backsberg.component.scss']
})
export class TreffenBacksbergComponent implements OnInit {

  constructor(meta: Meta, title: Title) {

    title.setTitle('Einladung Treffen - Bremer-Italo-Club');

    meta.addTags([
      { name: 'author', content: 'unbekannt3' },
      { name: 'keywords', content: 'BIC, Motorräder, Bremen, Bremer-Italo-Club, Italien, Treffen, Backsberg, Oyten, Fischerhude, Motorradtreffen' },
      { name: 'description', content: 'Einladung zum 27. Motorradtreffen des Bremer-Italo-Clubs 2020' }
    ]);

  }

  ngOnInit() {
  }

}
